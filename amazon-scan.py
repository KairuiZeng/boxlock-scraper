"""Get a list of Messages from the user's mailbox.
"""
from __future__ import print_function
import httplib2
import os
import requests
from bs4 import BeautifulSoup

from apiclient import discovery
from oauth2client import client
from oauth2client import tools
from oauth2client.file import Storage
from apiclient import errors

import base64
import email
import quopri
from html.parser import HTMLParser

try:
    import argparse
    flags = argparse.ArgumentParser(parents=[tools.argparser]).parse_args()
except ImportError:
    flags = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/gmail-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/gmail.readonly'
CLIENT_SECRET_FILE = 'client_secret.json'
APPLICATION_NAME = 'Gmail Amazon Scanner'


def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'gmail-python-quickstart.json')

    store = Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        if flags:
            credentials = tools.run_flow(flow, store, flags)
        else:  # Needed only for compatibility with Python 2.6
            credentials = tools.run(flow, store)
        print('Storing credentials to ' + credential_path)
    return credentials


def ListMessagesMatchingQuery(service, user_id, query='', maxr=0):
    """List all Messages of the user's mailbox matching the query.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      query: String used to filter messages returned.
      Eg.- 'from:user@some_domain.com' for Messages from a particular sender.

    Returns:
      List of Messages that match the criteria of the query. Note that the
      returned list contains Message IDs, you must use get with the
      appropriate ID to get the details of a Message.
    """
    try:
        response = service.users().messages().list(userId=user_id,
                                                   q=query, maxResults=maxr).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])
        '''
        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(userId=user_id, q=query,
                                                       pageToken=page_token).execute()
            messages.extend(response['messages'])
        '''
        return messages
    except errors.HttpError as err:
        print("http error")


def ListMessagesWithLabels(service, user_id, label_ids=[]):
    """List all Messages of the user's mailbox with label_ids applied.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      label_ids: Only return Messages with these labelIds applied.

    Returns:
      List of Messages that have all required Labels applied. Note that the
      returned list contains Message IDs, you must use get with the
      appropriate id to get the details of a Message.
    """
    try:
        response = service.users().messages().list(userId=user_id,
                                                   labelIds=label_ids).execute()
        messages = []
        if 'messages' in response:
            messages.extend(response['messages'])

        while 'nextPageToken' in response:
            page_token = response['nextPageToken']
            response = service.users().messages().list(userId=user_id,
                                                       labelIds=label_ids,
                                                       pageToken=page_token).execute()
            messages.extend(response['messages'])

        return messages
    except errors.HttpError as err:
        print("http error")


def GetMessage(service, user_id, msg_id):
    """Get a Message with given ID.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A Message.
    """
    try:
        message = service.users().messages().get(userId=user_id, id=msg_id).execute()
        # print('Message snippet: %s' % message['snippet'])
        # print ('Inbox found')

        return message
    except errors.HttpError as error:
        print('An error occurred: %s' % error)


def GetMimeMessage(service, user_id, msg_id):
    """Get a Message and use it to create a MIME Message.

    Args:
      service: Authorized Gmail API service instance.
      user_id: User's email address. The special value "me"
      can be used to indicate the authenticated user.
      msg_id: The ID of the Message required.

    Returns:
      A MIME Message, consisting of data from Message.
    """
    try:
        # message = service.users().messages().get(userId=user_id, id=msg_id,
        #                                          format='raw').execute()
        #
        # print ('Message snippet: %s' % message['snippet'])
        #
        # msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
        #
        # mime_msg = email.message_from_string(msg_str)
        #
        # return mime_msg
        message = service.users().messages().get(userId=user_id, id=msg_id, format='raw').execute()
        # print('Message snippet: %s' % message['snippet'])
        # print (message['raw'].encode('ASCII'))
        msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII')).decode("utf-8")
        mime_msg = email.message_from_string(msg_str)

        for part in mime_msg.walk():
            # print(part.get_content_type())
            mime_msg.get_payload()
            if part.get_content_type() == 'text/html':
                mytext = part.get_payload() # TODO: decode mytext into normal text use beautiful soup?
                # print part.get_payload()
                # mytext2 = HTMLParser.feed(mytext)
                mytext2 = BeautifulSoup(mytext, 'html')
                print(mytext2)

                reform = quopri.decode(mytext)

    except errors.HttpError as err:
        print('An error occurred: %s' % err)
    return reform


def main():
    """Shows basic usage of the Gmail API.

    Creates a Gmail API service object and outputs a list of label names
    of the user's Gmail account.
    """
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('gmail', 'v1', http=http)

    messagelist = ListMessagesMatchingQuery(
        service, 'me', 'from:shipment-tracking@amazon.com', maxr=1000)

    amazonID = ''
    if not messagelist:
        print('No messages found.')
    else:

        # for id in messagelist:
        #    print(id['id'])
    #  print('Messages:')
        msgNum = 0
        for message in messagelist:
            amazonID = messagelist[msgNum]['id']  # FIX THIS SO IT WORKS FOR NOT JUST FIRST ELEMENT
            #   print(label['name'])
            #   newMessage = GetMessageBody(service, 'me', amazonID)
            message = GetMessage(service, "me", amazonID)
            # message = GetMimeMessage(service, "me", amazonID)

        # try:
        # if 'payload' is not None:
            payld = message['payload']
            mssg_parts = payld['parts']  # fetching the message parts
            part_one = mssg_parts[0]  # fetching first element of the part
            part_body = part_one['body']  # fetching body of the message
            part_data = part_body['data']  # fetching data from the body
            # decoding from Base64 to UTF-8
            clean_one = part_data.replace("-", "+")
            # decoding from Base64 to UTF-8
            clean_one = clean_one.replace("_", "/")
            # decoding from Base64 to UTF-8
            clean_two = str(base64.b64decode(bytes(clean_one, 'UTF-8')))
            # in case people
            index_of_track = 0
            if clean_two.find("https://www.amazon.com/gp/css/shiptrack/view.html") != -1:
                index_of_track = clean_two.find("https://www.amazon.com/gp/css/shiptrack/view.html")
            elif clean_two.find("https://smile.amazon.com/gp/css/shiptrack/view.html") != -1:
                index_of_track = clean_two.find("https://smile.amazon.com/gp/css/shiptrack/view.html")

            track = clean_two[index_of_track:]
            pckgID = track.find("packageId=") + 10
            #find end of packageId
            notId = False
            while not notId:
                if not track[pckgID].isdigit():
                    notId = True
                else:
                    pckgID += 1
            finalURL = track[:(pckgID+1)]
            amazonScrape(finalURL)
            msgNum += 1
            # except KeyError:
            # else:
            #     print('No deliveries found.')


def amazonScrape(amzURL):
    page = requests.get(amzURL)

    if (page.status_code == 200):
        soup = BeautifulSoup(page.content, 'html.parser')

    # print("Begin scrape")

    '''
    Status
    '''
    packagestatus = soup.find(
        'span', class_='latest-event-status').get_text().rstrip().split()
    print("The package is currently: " + ' '.join(packagestatus)) # TODO: delayed packages not reflected in package status

    '''
    Tracking #
    '''
    tracklist = soup.find(
        'div', class_='ship-track-grid-subtext').get_text().split()
    print("The tracking number is: " +
          tracklist[len(tracklist) - 1] + " from the carrier " + tracklist[1].strip(','))


if __name__ == '__main__':
    main()
